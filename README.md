# NAME

cloudns - Interact with ClouDNS from CLI

# VERSION

The version can be retrieved with sub-command `version`:

```
$ cloudns version
```

# USAGE

```perl
cloudns [options] sub-command [sub-options]
```

# EXAMPLES

```perl
# Getting help is easy enough...
$ cloudns
$ cloudns commands
$ cloudns help add
$ cloudns add help  # same as previous

# In general you will want to set the credentials and the domain you
# want to work on
$ export CLOUDNS_CREDENTIALS='sub-auth-user username password'
$ export CLOUDNS_DOMAIN='example.com'

# Add a record in the domain
$ cloudns add a --host foo --record 10.20.30.40

# Adding a record prints out the identifier of the record itself
$ id="$(cloudns add a --host bar --record 127.0.0.1)"

# Deleting a record
$ cloudns rm "$id"

# List all available records, whatever their number
$ cloudns ls --all

# Look for specific hosts
$ cloudns ls --host bar

# Look for hosts, with a regex
$ cloudns ls --host /bar/

# Dump whole zone as a BIND file
$ cloudns export

# There's more than one way to pass the domain
$ cloudns --domain foo.com ls
$ cloudns ls --domain bar.com

# Some commands have even more
$ cloudns export baz.com
```

# DESCRIPTION

This program is a think wrapper about a selected part of the `ClouDNS`
API. See the entry point at [https://www.cloudns.net/wiki/article/56/](https://www.cloudns.net/wiki/article/56/).

The program has an extensive documentation system by just asking
`help`, so by all means everybody is encouraged to use it.

# CONFIGURATION

In addition to the configurations that you get by asking for `help`, it
is possible to set environment variable `CLOUDNS_DUMP` to print out the
whole configuration that will be passed to the `POST` request sent to
the API endpoint.

# DEPENDENCIES

This program depends on some modules, find them at
[https://gitlab.com/polettix/cloudns/-/blob/main/cpanfile](https://gitlab.com/polettix/cloudns/-/blob/main/cpanfile).

# BUGS AND LIMITATIONS

Please report any bugs or feature requests through the repository at
[https://gitlab.com/polettix/cloudns](https://gitlab.com/polettix/cloudns).

# AUTHOR

Flavio Poletti

# LICENSE AND COPYRIGHT

Copyright 2021 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

```
http://www.apache.org/licenses/LICENSE-2.0
```

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
